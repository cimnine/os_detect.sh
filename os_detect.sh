#!/bin/sh

# According http://semver.org/
v="1.0.0"

lowercase(){
	echo "$1" | sed "y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/"
}

####################################################################
# Get System Info
####################################################################
shootProfile(){
	OS=`lowercase \`uname\``
	KERNEL=`uname -r`
	MACH=`uname -m`

	if [ "${OS}" = "windowsnt" ]; then
		OS=windows
	elif [ "${OS}" = "darwin" ]; then
		OS=mac
	else
		OS=`uname`
		if [ "${OS}" = "SunOS" ] ; then
			OS=Solaris
			ARCH=`uname -p`
			OSSTR="${OS} ${REV}(${ARCH} `uname -v`)"
		elif [ "${OS}" = "AIX" ] ; then
			OSSTR="${OS} `oslevel` (`oslevel -r`)"
		elif [ "${OS}" = "Linux" ] ; then
			if [ -f /etc/redhat-release ] ; then
				DistroBasedOn='RedHat'
				DIST=`cat /etc/redhat-release |sed s/\ release.*//`
				PSUEDONAME=`cat /etc/redhat-release | sed s/.*\(// | sed s/\)//`
				REV=`cat /etc/redhat-release | sed s/.*release\ // | sed s/\ .*//`
			elif [ -f /etc/SuSE-release ] ; then
				DistroBasedOn='SuSe'
				PSUEDONAME=`cat /etc/SuSE-release | tr "\n" ' '| sed s/VERSION.*//`
				REV=`cat /etc/SuSE-release | tr "\n" ' ' | sed s/.*=\ //`
			elif [ -f /etc/mandrake-release ] ; then
				DistroBasedOn='Mandrake'
				PSUEDONAME=`cat /etc/mandrake-release | sed s/.*\(// | sed s/\)//`
				REV=`cat /etc/mandrake-release | sed s/.*release\ // | sed s/\ .*//`
			elif [ -f /etc/debian_version ] ; then
				DistroBasedOn='Debian'
				if [ -f /etc/lsb-release ] ; then
			        	DIST=`cat /etc/lsb-release | grep '^DISTRIB_ID' | awk -F=  '{ print $2 }'`
			                PSUEDONAME=`cat /etc/lsb-release | grep '^DISTRIB_CODENAME' | awk -F=  '{ print $2 }'`
			                REV=`cat /etc/lsb-release | grep '^DISTRIB_RELEASE' | awk -F=  '{ print $2 }'`
            			fi
			fi
			if [ -f /etc/UnitedLinux-release ] ; then
				DIST="${DIST}[`cat /etc/UnitedLinux-release | tr "\n" ' ' | sed s/VERSION.*//`]"
			fi
			OS=`lowercase $OS`
			DistroBasedOn=`lowercase $DistroBasedOn`
		 	#readonly OS
		 	#readonly DIST
			#readonly DistroBasedOn
		 	#readonly PSUEDONAME
		 	#readonly REV
		 	#readonly KERNEL
		 	#readonly MACH
		 	export OS
		 	export DIST
			export DistroBasedOn
		 	export PSUEDONAME
		 	export REV
		 	export KERNEL
		 	export MACH
		fi
	fi
}
shootProfile

printdebug() {
	echo "OS: $OS"
	echo "DIST: $DIST"
	echo "PSUEDONAME: $PSUEDONAME"
	echo "REV: $REV"
	echo "DistroBasedOn: $DistroBasedOn"
	echo "KERNEL: $KERNEL"
	echo "MACH: $MACH"
}

printhelp() {
	echo "Usage: `basename ${0}` [ -h | -v | -d ]"
	echo "   -h: help"
	echo "   -v: version"
	echo "   -d: debug"
}

version() {
	echo "Version: ${v}"
	echo 
	cat <<EOL
os_detect.sh  Copyright (C) 2015  Christian Mäder
This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it
under certain conditions.
For details read LICENSE or visit http://www.gnu.org/licenses/gpl-3.0-standalone.html
EOL
}

option="${1}"
case ${option} in
	-h*)
		printhelp
		;;
	-v*)
		version
		;;
	-d*)
		printdebug
		;;
esac
