# os_detect.sh

A "pure" shell script to detect the OS on which the script gets executed.

This scripts was forked on the 6th of August 2015 from https://raw.githubusercontent.com/coto/server-easy-install/master/lib/core.sh (dba266ad681cd1440013f31bac3d6230a0f9b271).

# Aims

* Pure SH script
* Reliable detection
* Fast detection

# Features

* Exports the current OS as $OS
* If possible, exports $KERNEL version
* If possible, exports $MACH
* If possible, exports $DIST

# License

As the original file was licensed under GPL, this derivate has to licensed under GPL as well. As it was not specified, which version of the license shall be applicable, GPLv3 or later is assumed.
